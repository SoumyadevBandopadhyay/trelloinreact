import React, { Component } from 'react'
import Cards from './Cards'

const key="7b3d76bb1b532e2ca3cf746cae30179b"
const token= "65046ef2e1e55fdef9a5e9bec076054c8103b66cc04dac67f31b5eaad811114b"


class EachList extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             dropdown:false,
        }
        this.archiveList=this.archiveList.bind(this)
    }
    
    archiveList(){
        this.props.archiveList(this.props.listId)
    }

    onClickListMenu=()=>{
        this.setState({
            dropdown:!this.state.dropdown
        })
    }

    render() {
        return (
            <div className="level-item eachList">
                <div style={{flexGrow:"inherit",}}>
                    <div style={{display:"flex", justifyContent:"space-between"}}>
                        <p className="eachListHeading">{this.props.name}</p>

                        <div className="dropdown is-active">
                            <div className="dropdown-trigger">
                                <button onClick={this.onClickListMenu} className="button" aria-haspopup="true" aria-controls="dropdown-menu">
                                    <i className="fas fa-ellipsis-v" aria-hidden="true"></i>
                                </button>
                            </div>
                            {this.state.dropdown
                            ?
                            <div className="dropdown-menu" id="dropdown-menu" role="menu">
                                <div className="dropdown-content">
                                <button onClick={this.archiveList}>
                                    <span>
                                        Archive list
                                    </span>
                                </button>
                                </div>
                            </div>
                            :<div id="dropdown-menu" role="menu"></div>
                            }
                        </div>
                    </div>
                    {/* <div>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Est deleniti quaerat totam aliquid blanditiis itaque nisi neque dolorem voluptatem. Nostrum esse laborum ratione officia maiores ipsum magni voluptates maxime rem?
                    </div> */}
                    <Cards listId={this.props.listId}/>
                </div>
            </div>
        )
    }
}

export default EachList


