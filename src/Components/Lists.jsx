import React, { Component } from 'react'
import EachList from './EachList'
import CreateList from './CreateList'


const key="7b3d76bb1b532e2ca3cf746cae30179b"
const token= "65046ef2e1e55fdef9a5e9bec076054c8103b66cc04dac67f31b5eaad811114b"

class Lists extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            allLists:[], 
        }
        this.addList= this.addList.bind(this)
        this.archiveList=this.archiveList.bind(this)
    }
    
    componentDidMount(){
        fetch(`https://api.trello.com/1/boards/${this.state.boardId}/lists?key=${key}&token=${token}`, {
            method: 'GET'
        })
        .then(response => {
            return response.json();
        })
        .then(result => {
            // console.log(result)
            this.setState({
                allLists: result,
            })
        })
        .catch(err => console.error(err));
    }

    addList(name){
        fetch(`https://api.trello.com/1/boards/${this.state.boardId}/lists?name=${name}&key=${key}&token=${token}`, {
        method: 'POST'
        })
            .then(response => {
                console.log(
                `Response: ${response.status} ${response.statusText}`
                );
                return response.json();
            })
            .then(result => {
                this.setState({
                    allLists:[...this.state.allLists,result]
                })
            })

            .catch(err => console.error(err));
    }

    archiveList(id){
        fetch(`https://api.trello.com/1/lists/${id}/closed?value=true&token=${token}&key=${key}`, {
            method: 'PUT'
                })
                .then(response => {
                    console.log(
                    `Response: ${response.status} ${response.statusText}`
                    );
                    return response.json();
                })
                .then(result =>{
                    console.log(result.id)
                    this.setState({
                        allLists: this.state.allLists.filter(list=>list.id!==result.id)
                    })
                })
                .catch(err => console.error(err));
    }

    static getDerivedStateFromProps(props,state){
        return({
            boardId: props.match.params.boardId,
        })
    }


    render() {
        return (
            <nav className="level is-mobile allList">
                {this.state.allLists.map((eachlist)=>{
                    return(
                        <EachList listId={eachlist.id} key={eachlist.id} name={eachlist.name} archiveList={this.archiveList}/>
                    )
                })}
                <CreateList addList={this.addList}/>
            </nav>
        )
    }
}

export default Lists
