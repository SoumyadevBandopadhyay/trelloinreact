import React, { Component } from 'react'
import Modal from './Modal'

const key="7b3d76bb1b532e2ca3cf746cae30179b"
const token= "65046ef2e1e55fdef9a5e9bec076054c8103b66cc04dac67f31b5eaad811114b"

class EachCard extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
           editVar:false, 
           nameval:this.props.name, 
           cardAppearance:"",
           displayModal:false,

        }
        this.onEdit=this.onEdit.bind(this)
        this.editChange=this.editChange.bind(this)
        this.onSubmitChangedEdit=this.onSubmitChangedEdit.bind(this)
        this.deleteCard=this.deleteCard.bind(this)
        this.onCardClick=this.onCardClick.bind(this)
        this.onMouseEnterCard=this.onMouseEnterCard.bind(this)
        this.onMouseLeaveCard=this.onMouseLeaveCard.bind(this)
        this.closeModal=this.closeModal.bind(this)
    }

    onEdit(){
        this.setState({
            editVar:!this.state.editVar,
        })
    }
    
    editChange(event){
        this.setState({
            nameval:event.target.value,
        })
        // console.log("yyy")
    }

    onSubmitChangedEdit(event){
        event.preventDefault()
        this.props.editCardName(this.state.nameval, this.props.cardId)
        console.log(this.state.nameval)
        this.setState({
            editVar:!this.state.editVar,
        })
    }

    deleteCard(){
        if(prompt("Enter Password to delete card")==="Soms"){
            this.props.deleteCard(this.props.cardId)
            fetch(`https://api.trello.com/1/cards/${this.props.cardId}?key=${key}&token=${token}`, {
            method: 'DELETE'
            })
                .then(response => {
                    console.log(
                    `Response: ${response.status} ${response.statusText}`
                    );
                    return response.json();
                })
                .then(result => console.log(result))
                .catch(err => console.error(err));
        }else{
            alert("Wrong Password")
        }
    }

    onCardClick(){
       this.setState({
           displayModal:true,
       }) 
    }

    onMouseEnterCard(){
        this.setState({
            cardAppearance:"cardHover"
        })
    }

    onMouseLeaveCard(){
        this.setState({
            cardAppearance:"",
        })
    }

    closeModal(val){
        this.setState({
            displayModal:!this.state.displayModal,
        })
    }

    render() {
        return (
            <div>
                <div onClick={this.onCardClick} onMouseEnter={this.onMouseEnterCard} onMouseLeave={this.onMouseLeaveCard}>
                    {this.state.editVar===false ? 
                        <div className={this.state.cardAppearance + " eachCard"}>
                            {this.state.nameval}
                            <div style={{display:"flex",justifyContent:"flex-end", opacity:"0.5"}}>
                            <button onClick={this.onEdit}>
                                <i className="fas fa-pencil-alt"></i>
                            </button>
                            <button onClick={this.deleteCard}>
                                <i className="fas fa-trash-alt"></i>
                            </button>
                            </div>
                        </div> 
                        :
                        <form onSubmit={this.onSubmitChangedEdit} className="eachCard">
                          <input onChange={this.editChange} value={this.state.nameval}/>  
                        </form>
                    }
                </div>
                <Modal key={this.props.cardId} displayModal={this.state.displayModal} closeModal={this.closeModal} {...this.props.cardInfo}/>
            </div>
        )
    }
}

export default EachCard
