import React, { Component } from 'react'
import {Link} from 'react-router-dom'

    class EachBoard extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
      
    deleteTheBoard=()=>{
        this.props.deleteBoard(this.props.id)
    }

    render() {
        // console.log(this.props)
        return (
            // <Link to={"/boards/"+this.props.id}>
                <div className="tile is-parent box" style={{backgroundColor:"transparent", backgroundImage: `url(${this.props.background})` }}>
                    <Link to={"/boards/"+this.props.id}>
                    <article className="tile is-child box" style={{backgroundColor:"transparent" }}>
                        <p className="sub-title">{this.props.name}</p>
                    </article>
                    </Link>                   
                    <div className="media-right">
                        <button onClick={this.deleteTheBoard} className="delete"></button>
                    </div> 
                </div>
            
        )
    }
}

export default EachBoard
