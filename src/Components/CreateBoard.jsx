import React, { Component } from 'react'


class CreateBoard extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            checkvar: false,
            value: "",
        }
        this.onClick=this.onClick.bind(this)
        this.onSubmit=this.onSubmit.bind(this)
    }
    
    onClick(event){
        this.setState({
            checkvar: true,
        })
    }
    onSubmit(event){
       event.preventDefault()
       this.props.addBoard(this.state.value)
       this.setState({
           value: "",
           checkvar:false,
       })
    }

    onChange=(event)=>{
        this.setState({
            value: event.target.value,
        })
    }

    render() {
        return (
            <div onClick={this.onClick} className="tile is-parent">
                <article className="tile is-child box">
                {(this.state.checkvar===false)?<p className="sub-title" style={{opacity:"0.5"}}>Click To Add Card</p> : <form onSubmit={this.onSubmit}><input onChange={this.onChange} value={this.state.value} className="input" type="text" placeholder="Name Card"/></form>}
                </article>
            </div>
        )
    }
}

export default CreateBoard