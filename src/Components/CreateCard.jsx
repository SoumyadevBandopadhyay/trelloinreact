import React, { Component } from 'react'

class CreateCard extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             cardName:"",
        }
        this.onSubmitCard=this.onSubmitCard.bind(this)
        this.onChange=this.onChange.bind(this)
    }
    
    onSubmitCard(event){
        console.log("Inside Submit of Create Card")
        event.preventDefault()
        this.props.addCard(this.state.cardName)
        event.target[0].value=""
    }

    onChange(event){
        this.setState({
            cardName:event.target.value,
        })
    }

    render() {
        return (
            <div className="eachCard">
               <form onSubmit={this.onSubmitCard} className="eachCard">
                   <input onChange={this.onChange} style={{fontSize:"1rem"}} placeholder="+ Add Card" />
               </form> 
            </div>
        )
    }
}

export default CreateCard
