import React, { Component } from 'react'
import EachCard from './EachCard'
import CreateCard from './CreateCard'
import Modal from './Modal'

const key="7b3d76bb1b532e2ca3cf746cae30179b"
const token= "65046ef2e1e55fdef9a5e9bec076054c8103b66cc04dac67f31b5eaad811114b"

class Cards extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            allCards:[], 
    
        }
        this.editCardName=this.editCardName.bind(this)
        this.deleteCard=this.deleteCard.bind(this)
        this.addCard=this.addCard.bind(this)
    }
    
    componentDidMount(){
        fetch(`https://api.trello.com/1/lists/${this.props.listId}/cards?key=${key}&token=${token}`, {
            method: 'GET'
        })
            .then(response => {
                console.log(
                `Response: ${response.status} ${response.statusText}`
                );
                return response.json();
            })
            .then(result => {
                console.log(result)
                this.setState({
                    allCards:result,
                })
            })
            .catch(err => console.error(err)); 
    }

    editCardName(name,id){
        this.setState({
            allCards:this.state.allCards.map((eachCard)=>{
                if(eachCard.id===id){
                    eachCard.name=name
                }
                return eachCard
            })
        })
        fetch(`https://api.trello.com/1/cards/${id}?name=${name}&key=${key}&token=${token}`, {
        method: 'PUT',
        })
            .then(response => {
                console.log(
                `Response: ${response.status} ${response.statusText}`
                );
                return response.json();
            })
            .then(result => console.log(result))
            .catch(err => console.error(err));
    }

    deleteCard(id){
       this.setState({
           allCards: this.state.allCards.filter(eachCard=>eachCard.id!==id)
       }) 
    }

    addCard(name){
        fetch(`https://api.trello.com/1/cards?idList=${this.props.listId}&name=${name}&key=${key}&token=${token}`, {
            method: 'POST'
          })
            .then(response => {
              console.log(
                `Response: ${response.status} ${response.statusText}`
              );
              return response.json();
            })
            .then((result) => {
                console.log(result)
                this.setState({
                    allCards:[...this.state.allCards, result]
                })
            })
            .catch(err => console.error(err));
    }


    render() {
        return (
            <div>
              {this.state.allCards.map((eachCard)=>{
                // console.log(eachCard)
                return (
                <div key={eachCard.id}>
                    <EachCard name={eachCard.name} key={eachCard.id} cardId={eachCard.id} cardInfo={eachCard}/>
                </div>
                )
            })}
            <CreateCard addCard={this.addCard}/>
            </div>
        )
    }
}

export default Cards
