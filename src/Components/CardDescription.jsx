import React, { Component } from 'react'

class CardDescription extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             desc:"",
             checkVar:false,
        }
    }
    

    static getDerivedStateFromProps(props){
        return({
            desc: props.desc,
        })
    }

    render() {
        return (
            <div>
                <div style={{display:"flex",justifyContent:"space-between"}}>
                    <h4>Description</h4>
                    {this.state.desc.length===0?
                    <div></div>
                    :
                    <button>
                        Edit
                    </button>
                    }
                </div>
                <div style={{display:"flex",border:"solid"}} className="box" >
                    {this.state.desc.length===0?
                    <form style={{border:"solid",}}>
                        <textarea class="textarea" placeholder="Add Card Description" style={{flexBasis:"5rem"}}></textarea>
                    </form>
                    :
                    <p style={{flexWrap:"wrap"}}>{this.state.desc}</p>
                    }
                </div>
            </div>
        )
    }
}

export default CardDescription
