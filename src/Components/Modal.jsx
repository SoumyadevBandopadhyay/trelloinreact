import React, { Component } from 'react'
import CardDescription from './CardDescription'

class Modal extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            modalClass:"modal" 
        }
        this.closeModal=this.closeModal.bind(this)
    }
    
    static getDerivedStateFromProps(props,state){
        if(props.displayModal===true){
            return({
                modalClass: state.modalClass+" is-active"
            })
        }else{
            return null
        }
    }
    
    closeModal(){
        console.log("Inside Close MOdal")
        this.props.closeModal(false)
        this.setState({
            modalClass:"modal",
        })
    }

    render() {
        return (
            <div className={this.state.modalClass}>
            <div className="modal-background modal-background"></div>
            <div className="modal-card">
                <header className="modal-card-head">
                <p className="modal-card-title">{this.props.name}</p>
                <button onClick={this.closeModal} className="delete" aria-label="close"></button>
                </header>
                <section className="modal-card-body">
                <CardDescription desc={this.props.desc}/>
                {/* <!-- Content ... --> */}
                </section>
            </div>
            </div>
        )
    }
}

export default Modal
