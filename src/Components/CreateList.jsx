import React, { Component } from 'react'

class CreateList extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             listName:"",
        }
        this.onSubmit=this.onSubmit.bind(this)
        this.onChange=this.onChange.bind(this)
    }
    onChange(event){
        this.setState({
            listName:event.target.value
        })
    }

    onSubmit(event){
        event.preventDefault()
        this.props.addList(this.state.listName)
        this.setState({
            listName:"",
        })
        event.target[0].value=""
    }

    render() {
        return (
            <form onSubmit={this.onSubmit} className="level-item eachList" >
                <input onChange={this.onChange} className="eachListHeading" placeholder="+ Add Another List" />           
            </form>
        )
    }
}

export default CreateList
