import React, { Component } from 'react'
import EachBoard from './EachBoard'
import CreateBoard from './CreateBoard'
import {Link, NavLink, withRouter, Route, BrowserRouter} from 'react-router-dom'
import Lists from './Lists'

const key="7b3d76bb1b532e2ca3cf746cae30179b"
const token= "65046ef2e1e55fdef9a5e9bec076054c8103b66cc04dac67f31b5eaad811114b"

class Boards extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            allBoards:[],
        }
    }

    componentDidMount(){
        fetch(`https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`)
        .then(res=>res.json())
        .then((result)=>{
            // console.log(result)
            this.setState({
                allBoards: result,
            })
        }).catch(err=>console.log(err))
    }

    addBoard=(name)=>{
        fetch(`https://api.trello.com/1/boards/?name=${name}&key=${key}&token=${token}`, {
        method: 'POST'
        })
        .then(response => {
            console.log(
            `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
        })
        .then(result => {
            // console.log(result)
            this.setState({
                allBoards: [...this.state.allBoards,result],
            })
        })

        .catch(err => console.error(err));
    }

    deleteBoard=(id)=>{
        this.setState({
            allBoards:this.state.allBoards.filter((board)=>board.id!==id)
        })
    }

    render() {
        return (
            <div className="tile is-ancestor boardList">
                {this.state.allBoards.map((boards) => {
                   return (
                            <EachBoard key={boards.id} id={boards.id} name={boards.name} background={boards.prefs.backgroundImage} deleteBoard={this.deleteBoard} />                       
                   )
                }
                )}
                <CreateBoard addBoard={this.addBoard} />
            </div>

        )
    }
}

export default  Boards
