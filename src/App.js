import React, { Component } from 'react'
import Boards from './Components/Boards'
import EachBoard from './Components/EachBoard'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import Home from './Components/Home'
import Lists from './Components/Lists'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
          <Route exact path="/boards" component={Boards}/>
          <Route exact path="/" component={Home}/>
          <Route path="/boards/:boardId" component={Lists}/>
      </BrowserRouter>
    )
  }
}

export default App


